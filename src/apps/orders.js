const express = require('express');
const router = express.Router();
const fs = require('fs');
const database = JSON.parse(fs.readFileSync('database.json'));

// Create a new order
router.post('/create', (req, res) => {
    const { email, productId, quantity } = req.body;
 
    if (!email || !productId || !quantity) {
      return res.status(400).json({ message: 'Missing required fields' });
    }
 
    const user = database.users[email];
 
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }
 
    const product = database.products[productId];
 
    if (!product) {
      return res.status(404).json({ message: 'Product not found' });
    }
 
    const id = Date.now().toString();
 
    const data = { id, email, productId, quantity };

    database.users.push(data);

    fs.writeFile('database.json', JSON.stringify(database), err => {
      if (err) {
        console.error(err);
        return;
      }
      console.log('Data written to file');
    });
 
    res.status(201).json({ message: 'Order created', id });
});
 
// Get a list of all orders
router.get('/all', (req, res) => {
    const orders = Object.values(database.orders).map(order => {
      const user = database.users[order.email];
      const product = database.products[order.productId];
 
      return { ...order, user, product };
    });
 
    res.json(orders);
});
 
// Get a specific order by id
router.get('/:id', (req, res) => {
    const order = database.orders[req.params.id];
 
    if (!order) {
      return res.status(404).json({ message: 'Order not found' });
    }
 
    const user = database.users[order.email];
    const product = database.products[order.productId];
 
    res.json({ ...order, user, product });
});
 
// Update an order by id
router.patch('/:id', (req, res) => {
    const order = database.orders[req.params.id];
 
    if (!order) {
      return res.status(404).json({ message: 'Order not found' });
    }
 
    Object.assign(order, req.body);
 
    res.json({ message: 'Order updated' });
});
 
// Delete an order by id
router.delete('/:id', (req, res) => {
    if (!database.orders[req.params.id]) {
      return res.status(404).json({ message: 'Order not found' });
    }
 
    delete database.orders[req.params.id];
 
    res.json({ message: 'Order deleted' });
});

module.exports = router;
 