const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const { generateToken } = require('./jwt');

router.post('/login', (req, res) => {
  const { email, password } = req.body;
  const user = database.users[email];

  if (!user) {
    return res.status(401).json({ message: 'Invalid email or password' });
  }

  bcrypt.compare(password, user.password, (err, result) => {
    if (err) {
      return res.status(500).json({ message: err.message });
    }

    if (!result) {
      return res.status(401).json({ message: 'Invalid email or password' });
    }

    const token = generateToken({ email: user.email });

    res.json({ token });
  });
});

module.exports = router;
