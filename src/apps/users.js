const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const fs = require('fs');
const database = JSON.parse(fs.readFileSync('database.json'));

// Create new user
router.post('/create', (req, res) => {
    const { email, password } = req.body;
  
    if (!email || !password) {
      return res.status(400).json({ message: 'Missing required fields' });
    }
  
    if (database.users[email]) {
      return res.status(409).json({ message: 'Email already exists' });
    }
  
    bcrypt.hash(password, 10, (err, hash) => {
      if (err) {
        return res.status(500).json({ message: err.message });
      }

      const data = { email, password: hash };

      database.users.push(data);

      fs.writeFile('database.json', JSON.stringify(database), err => {
        if (err) {
          console.error(err);
          return;
        }
        console.log('Data written to file');
      });
  
      res.status(201).json({ message: 'User created' });
    });
});
  
// Get a list of all users
router.get('/all', (req, res) => {
    res.json(Object.values(database.users));
});

 // Get a specific user by email
 router.get('/:email', (req, res) => {
    const user = database.users[req.params.email];
 
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }
 
    res.json(user);
});
 
// Update a user by email
router.patch('/:email', (req, res) => {
    const user = database.users[req.params.email];
 
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }
 
    if (req.body.email && req.body.email !== req.params.email && data.users[req.body.email]) {
      return res.status(409).json({ message: 'Email already exists' });
    }
 
    if (req.body.password) {
      bcrypt.hash(req.body.password, 10, (err, hash) => {
        if (err) {
          return res.status(500).json({ message: err.message });
        }
 
        user.password = hash;
        delete req.body.password;
 
        Object.assign(user, req.body);
 
        res.json({ message: 'User updated' });
      });
    } else {
      Object.assign(user, req.body);
 
      res.json({ message: 'User updated' });
    }
});
 
// Delete a user by email
router.delete('/:email', (req, res) => {
    if (!database.users[req.params.email]) {
        return res.status(404).json({ message: 'User not found' });
    }

    delete database.users[req.params.email];

    res.json({ message: 'User deleted' });
});

module.exports = router;
 
  