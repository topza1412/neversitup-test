const Joi = require('joi');


//ตอบ ข้อ1
const validate1 = Joi.object({
    input: Joi.string().min(6).required()
});

validate1.validate({ input: '172839'});


//ตอบ ข้อ2
const validate2 = Joi.object({
    input: Joi.string().pattern(/^(?!.*(\d)\1{2})\d+$/).required()
});

validate2.validate({ input: '111762'});


//ตอบ ข้อ3
const validate3 = Joi.object({
    input: Joi.string().pattern(/^(?!.*(\d)(?=(\d)\1{1})(?=(\d)\2{1})).+$/).required()
});

validate3.validate({ input: '124578'});


//ตอบ ข้อ4
const validate4 = Joi.object({
    input: Joi.string().custom((value, helpers) => {
        const regex = /^(?:(\d)(?!\1{2}))+$/;
        const isDuplicate = (value.match(regex) || []).length !== value.length / 3;
        if (isDuplicate) {
          return helpers.error('any.invalid');
        }
        return value;
    }, 'custom validation')
});

validate4.validate({ input: '887712'});
