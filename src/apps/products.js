const express = require('express');
const router = express.Router();
const fs = require('fs');
const database = JSON.parse(fs.readFileSync('database.json'));


// Create a new product
router.post('/create', (req, res) => {
    const { name, price } = req.body;
  
    if (!name || !price) {
      return res.status(400).json({ message: 'Missing required fields' });
    }
  
    const id = Date.now().toString();
  
    const data = { id, name, price };

    database.users.push(data);

    fs.writeFile('database.json', JSON.stringify(database), err => {
      if (err) {
        console.error(err);
        return;
      }
      console.log('Data written to file');
    });
  
    res.status(201).json({ message: 'Product created', id });
});
  
// Get a list of all products
router.get('/all', (req, res) => {
    res.json(Object.values(database.products));
  });
  
  // Get a specific product by id
  router.get('/:id', (req, res) => {
    const product = database.products[req.params.id];
  
    if (!product) {
      return res.status(404).json({ message: 'Product not found' });
    }
  
    res.json(product);
});
  
// Update a product by id
router.patch('/:id', (req, res) => {
    const product = database.products[req.params.id];
  
    if (!product) {
      return res.status(404).json({ message: 'Product not found' });
    }
  
    Object.assign(product, req.body);
  
    res.json({ message: 'Product updated' });
});
  
// Delete a product by id
router.delete('/:id', (req, res) => {
    if (!database.products[req.params.id]) {
      return res.status(404).json({ message: 'Product not found' });
    }
  
    delete database.products[req.params.id];
  
    res.json({ message: 'Product deleted' });
});

module.exports = router;
  