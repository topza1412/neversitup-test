const express = require('express');
const bodyParser = require('body-parser');
const port = 3000;
const app = express();
const auth = require('./src/apps/auth');
const users = require('./src/apps/users');
const products = require('./src/apps/products');
const orders = require('./src/apps/orders');

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', '*');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.send('API is running');
});

app.use('/auth', auth);

app.use('/users', users);

app.use('/products', products);

app.use('/orders', orders);

app.listen(port, () => {
    console.log('Starting node.js on port ' + port)
})